Black Cow Coding Task

Introduction

Presented project is a realisation of short take-home task by Black Cow – Show Me The Robots

Technologies and IDE used

The project was developed in Python using PyCharm IDE utilising test-driven development and Object-Oriented approach.
For testing pytest framework was used.

Considerations and Assumptions

Consideration were given to the amount of memory used by the program.
For this purpose memory_profiler package was used by adding @profile decorator to the methods and functions. No memory issues were detected.
The boundaries of the grid representing the asteroid are treated and tested in the Project.
The situation when if a new Robot could collide with already existing Robot when landing (a message stating the position of a new robot) or moving (a message telling the current robot to move) was appreciated but not treated for simplicity as not stated in the Task formulation. 

Program 	

To execute the program run the following command: Python main.py Input.txt

Worked example

Input (Input.txt)

{"type": "asteroid", "size": {"x": 5, "y": 5}}
{"type": "new-robot", "position": {"x": 1, "y": 2}, "bearing": "north"}
{"type": "move", "movement": "turn-left"}
{"type": "move", "movement": "move-forward"}
{"type": "move", "movement": "turn-left"}
{"type": "move", "movement": "move-forward"}
{"type": "move", "movement": "turn-left"}
{"type": "move", "movement": "move-forward"}
{"type": "move", "movement": "turn-left"}
{"type": "move", "movement": "move-forward"}
{"type": "move", "movement": "move-forward"}
{"type": "new-robot", "position": {"x": 3, "y": 3}, "bearing": "east"}
{"type": "move", "movement": "move-forward"}
{"type": "move", "movement": "move-forward"}
{"type": "move", "movement": "turn-right"}
{"type": "move", "movement": "move-forward"}
{"type": "move", "movement": "move-forward"}
{"type": "move", "movement": "turn-right"}
{"type": "move", "movement": "move-forward"}
{"type": "move", "movement": "turn-right"}
{"type": "move", "movement": "turn-right"}
{"type": "move", "movement": "move-forward"}

Output

{"type": "robot", "position": {"x": 1, "y": 3}, "bearing": "north"}
{"type": "robot", "position": {"x": 5, "y": 1}, "bearing": "east"}



